# catch

Catch Code Challange

Name                : Andi Huang
Phone No (Whatsapp) : +6282284274728

System Design:
I create an IExportOrdersGenerator contract as a contract for all Export Order file Generator.
There is 3 File Generator in my implementation, They are for creating csv, json and yaml files.
The framework I use is Laravel.
The package I include in this project are:
1. aws/aws-sdk-php
2. league/csv
3. symfony/yaml

I was using S3Client and call the registerStreamWrapper() function so i can get the stream line by line and able process larger data.

How to run the program:
1. Clone the repository from git@gitlab.com:tianwen/catch.git or https://gitlab.com/tianwen/catch.git.
2. cd into the directory and Run composer install --no-dev
3. Copy the .env file from my attachment and setup:
    DB_CONNECTION=mysql
    DB_HOST
    DB_PORT
    DB_DATABASE
    DB_USERNAME
    DB_PASSWORD
    MAIL_DRIVER
    MAIL_FROM_ADDRESS
    MAIL_FROM_NAME
    AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY
    AWS_DEFAULT_REGION
    AWS_SES_REGION
4. Run php artisan migrate.
5. Run php artisan serve(to serve through http and expose exports and records through API).
6. Run php artisan export:orders {email} {type}. To run the export.