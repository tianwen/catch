<?php

namespace App\Console\Commands;

use Aws\S3\S3Client;  
use Aws\Exception\AwsException;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Console\Commands\ExportOrdersGenerator\ExportOrdersGeneratorFactory;
use App\Models\Export;
use App\Models\Record;
use App\Models\Enums\ExportStatus;

use Carbon\Carbon;

use Illuminate\Console\Command;

class ExportOrders extends Command
{
    private $exportOrdersGeneratorFactory;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:orders {recipient_email} {type=csv}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ExportOrdersGeneratorFactory $exportOrdersGeneratorFactory)
    {
        parent::__construct();
        $this->exportOrdersGeneratorFactory = $exportOrdersGeneratorFactory;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $validator = Validator::make(
            [
                'recipient_email' => $this->argument('recipient_email'),
                'type' => $this->argument('type'),
            ],
            [
                'recipient_email' => 'required|email',
                'type' => 'required|in:csv,yaml,json',
            ],
            [
                'in'      => 'The :attribute must be one of the following types: :values',
            ]);
        if ($validator->fails())
        {
            echo 'Invalid Argument(s).'."\n";
            foreach($validator->errors()->getMessages() as $key => $value)
            {
                echo "\t".$key."\n";
                foreach($value as $message)
                    echo "\t\t".$message."\n";
            }
            return;
        }
        $client = new S3Client([
            'region' => 'ap-southeast-2',
            'version' => '2006-03-01',
            'credentials' => false //if got credentials fopen will try to use getObject and return access denied error
        ]);
        $client->registerStreamWrapper();
        $stream = fopen('s3://catch-code-challenge/challenge-1-in.jsonl', 'r');
        $generator = $this->exportOrdersGeneratorFactory->create($this->argument('type'));
        $filepath = $generator->createFile();
        $export = new Export();
        $export->recipient_email = $this->argument('recipient_email');
        $export->created_at = $export->freshTimestamp();
        $export->filename = basename($filepath);
        $export->type = $this->argument('type');
        $export->status = ExportStatus::IN_PROGRESS;
        $export->saveOrFail();
        try
        {
            $generator->startWrite();
            while (($line = fgets($stream)) !== false)
            {
                $o = json_decode($line);
                $order_total = 0;
                $order_count = 0;
                foreach ($o->items as $item)
                {
                    $order_count += $item->quantity;
                    $order_total += $item->quantity * $item->unit_price;
                }
                $order_discount = 0;
                foreach($o->discounts as $discount)
                {
                    switch ($discount->type) {
                        case 'DOLLAR':
                            $order_discount += $discount->value;
                            break;
                        
                        case 'PERCENTAGE':
                            $order_discount += round($order_total * $discount->value / 100.0,2);
                            break;
    
                        default:
                            throw new \Exception("Not implemented");
                            break;
                    }
                }
                $total_order_value = round($order_total - $order_discount, 2);
                if($total_order_value != 0)
                {
                    $datetime = strtotime($o->order_date);
                    $record = new \stdClass;
                    $record->order_id = $o->order_id;
                    $record->order_datetime = date(DATE_ISO8601,$datetime);
                    $record->total_order_value = $total_order_value;
                    $record->average_unit_price = round($total_order_value / $order_count,2);
                    $record->distinct_unit_count = count($o->items);
                    $record->total_units_count = $order_count;
                    $record->customer_state = $o->customer->shipping_address->state;
                    
                    $exportRecord = new Record();
                    $exportRecord->export_id = $export->id;
                    $exportRecord->order_id = $record->order_id;
                    $exportRecord->order_datetime = Carbon::parse($record->order_datetime);
                    $exportRecord->total_order_value = $record->total_order_value;
                    $exportRecord->average_unit_price = $record->average_unit_price;
                    $exportRecord->distinct_unit_count = $record->distinct_unit_count;
                    $exportRecord->total_units_count = $record->total_units_count;
                    $exportRecord->customer_state = $record->customer_state;
                    $exportRecord->saveOrFail();
    
                    $generator->writeRecord($record);
                }
            }
            fclose($stream);
            $generator->endWrite();
            $export->status = ExportStatus::SUCCESS;
            $export->saveOrFail();
            Mail::to(['email' => $this->argument('recipient_email')])->send(new \App\Mail\ExportOrders($filepath));
        }
        catch(\Exception $e)
        {
            var_dump($e->getMessage());
            $export->status = ExportStatus::ERROR;
            $export->saveOrFail();
        }        
    }
}
