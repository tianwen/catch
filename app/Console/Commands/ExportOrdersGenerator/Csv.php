<?php

namespace App\Console\Commands\ExportOrdersGenerator;

use League\Csv\CannotInsertRecord;
use League\Csv\Writer;
use App\Contracts\Console\Commands\IExportOrdersGenerator;

class Csv implements IExportOrdersGenerator
{
    private $writer;
    public function createFile()
    {
        $path = storage_path('app');
        $filename = time()."-orders.csv";
        $filepath = $path."/".$filename;
        $writer = Writer::createFromPath($filepath, 'w+');
        $this->writer = $writer;
        return $filepath;
    }
    public function startWrite()
    {
        $this->writer->insertOne([
            'order_id',
            'order_datetime',
            'total_order_value',
            'average_unit_price',
            'distinct_unit_count',
            'total_units_count',
            'customer_state',
        ]);
    }
    public function writeRecord($record)
    {                   
        try 
        {
            $this->writer->insertOne((array)$record);
        }catch (CannotInsertRecord $e) {
            $e->getRecords(); //returns [1, 2, 3]
        }
    }
    public function endWrite()
    {
        return;
    }
}