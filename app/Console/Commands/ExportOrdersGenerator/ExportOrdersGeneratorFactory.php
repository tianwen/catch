<?php

namespace App\Console\Commands\ExportOrdersGenerator;

class ExportOrdersGeneratorFactory
{
    public function create($type)
    {
        switch($type)
        {
            case 'csv':
                return new Csv();
                break;
            case 'yaml':
                return new Yaml();
                break;
            case 'json':
                return new Json();
                break;
        }
    }
}