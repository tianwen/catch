<?php

namespace App\Console\Commands\ExportOrdersGenerator;
use App\Contracts\Console\Commands\IExportOrdersGenerator;

class Json implements IExportOrdersGenerator
{
    private $writer;
    public function createFile()
    {
        $path = storage_path('app');
        $filename = time()."-orders.json";
        $filepath = $path."/".$filename;
        $writer = fopen($filepath, 'w+');
        $this->writer = $writer;
        return $filepath;
    }
    public function startWrite()
    {
        fwrite($this->writer, '{"orders":[');
    }
    public function writeRecord($record)
    {
        fwrite($this->writer, json_encode((array)$record).",\n");
    }
    public function endWrite()
    {
        fseek($this->writer, -2, SEEK_CUR);
        fwrite($this->writer, ']}');
    }
}