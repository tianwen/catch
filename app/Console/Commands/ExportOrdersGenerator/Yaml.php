<?php

namespace App\Console\Commands\ExportOrdersGenerator;
use Symfony\Component\Yaml\Yaml as Yamlencoder;
use App\Contracts\Console\Commands\IExportOrdersGenerator;

class Yaml implements IExportOrdersGenerator
{
    private $writer;
    public function createFile()
    {
        $path = storage_path('app');
        $filename = time()."-orders.yaml";
        $filepath = $path."/".$filename;
        $writer = fopen($filepath, 'w+');
        $this->writer = $writer;
        return $filepath;
    }
    public function startWrite()
    {
        return;
    }
    public function writeRecord($record)
    {
        fwrite($this->writer, Yamlencoder::dump(['order' => (array)$record]));
    }
    public function endWrite()
    {
        return;
    }
}