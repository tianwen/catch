<?php

namespace App\Contracts\Console\Commands;

interface IExportOrdersGenerator
{
    public function createFile();
    public function startWrite();
    public function writeRecord($record);
    public function endWrite();
}