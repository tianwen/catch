<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Export;
use Validator;

class ExportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exports = Export::orderBy('created_at', 'desc');
        return response()->json([
                'status' => 'ok',
                'data' => $exports->get()->toArray(),
            ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'recipient_email' => 'required|email',
                'type' => 'required|in:csv,yaml,json',
            ],
            [
                'in' => 'The :attribute must be one of the following types: :values',
            ]);
        if ($validator->fails())
            return response()->json([
                    'status' =>'error',
                    'message' => $validator->errors()
                ]);
        exec("php ../artisan export:orders ".$request->recipient_email." ". $request->type);
        return response()->json([
                'status' => 'ok',
            ]);
    }
}
