<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Record;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($export_id)
    {
        $records = Record::where('export_id', '=', $export_id);
        return response()->json([
                'status' => 'ok',
                'data' => $records->get()->toArray(),
            ]);
    }
}
