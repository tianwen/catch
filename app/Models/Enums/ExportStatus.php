<?php

namespace App\Models\Enums;
use \Lang;

final class ExportStatus {

	const IN_PROGRESS = 0;
	const SUCCESS = 1;
	const ERROR = 2;
	public static function getList() {
		return [self::IN_PROGRESS, self::SUCCESS, self::ERROR];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr)
			$result[] = [
				'id' => $arr,
				'description' => self::getString($arr),
			];
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case self::IN_PROGRESS:
				return Lang::get('enums.export_status_in_progress');
			case self::SUCCESS:
				return Lang::get('enums.export_status_success');
			case self::ERROR:
				return Lang::get('enums.export_status_error');
		}
	}
}
