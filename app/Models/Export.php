<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Enums\ExportStatus;

class Export extends Model
{
    public $timestamps = false;
    protected $visible = [
        'id',
        'filename',
        'recipient_email',
        'type',
        'created_at',
        'status_description'
    ];
    protected $appends = ['status_description'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:c',
    ];

    public function getStatusDescriptionAttribute()
    {
        return ExportStatus::getArray()[$this->status]['description'];
    }
    
}
