<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('export_id')->unsigned();
            $table->foreign('export_id')->references('id')->on('exports');
            $table->integer('order_id');
            $table->timestamp('order_datetime');
            $table->decimal('total_order_value', 18, 2);
            $table->decimal('average_unit_price', 18, 2);
            $table->integer('distinct_unit_count');
            $table->integer('total_units_count');
            $table->string('customer_state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
