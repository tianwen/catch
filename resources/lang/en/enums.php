<?php

return [
    //ExportStatus
    'export_status_in_progress' => 'In Progress',
    'export_status_success' => 'Success',
    'export_status_error' => 'Error',

];
